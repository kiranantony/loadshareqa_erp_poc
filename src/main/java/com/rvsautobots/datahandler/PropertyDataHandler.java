package com.rvsautobots.datahandler;

import java.io.InputStream;
import java.util.Properties;

import org.junit.Assert;

public class PropertyDataHandler {

	/**
	 * @Author AutobotsBDD Cucumber Team
	 * @Description :BDD
	 */
	public static final String strRootDir = System.getProperty("user.dir");
	public static String strReturnVal = null;

	/**
	 * Method to read values from PropertyFiles
	 * 
	 * @param fileName
	 *            and VariableName/KeywordName
	 * @returns String
	 **/

	public static String fetchPropertyValue(String fileName, String strVarName) {
		String propertyValue = null;
		try {

			Properties properties = new Properties();
			ClassLoader classLoader = PropertyDataHandler.class.getClassLoader();
			InputStream input = classLoader.getResourceAsStream(fileName + ".properties");
			properties.load(input);
			propertyValue = properties.getProperty(strVarName);

		} catch (

		Exception e) {
			Assert.fail("Property is not defined for : " + strVarName);

		}
		return propertyValue;
	}

}
