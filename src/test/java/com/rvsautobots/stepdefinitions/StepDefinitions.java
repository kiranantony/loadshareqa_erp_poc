package com.rvsautobots.stepdefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.rvsautobots.actions.UtilityActionHelper;
import com.rvsautobots.base.Autobase;
import com.rvsautobots.base.BrowserManager;
import com.rvsautobots.datahandler.PropertyDataHandler;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;


public class StepDefinitions extends Autobase {
	
	UtilityActionHelper UAObject = new UtilityActionHelper();
	
	
	 @Then("^click on the makeappointment button$")
	 public void click_on_the_makeappointment_button() throws Throwable {
		 
	    	WebElement makeapointButton=UAObject.getElementValue(driver,"Makeappointmentbutton" );
	    	makeapointButton.click();
	    	
	    }

	    @Then("^login to the application with \"([^\"]*)\" and \"([^\"]*)\"$")
	    public void login_to_the_application_with_something_and_something(String strArg1, String strArg2) throws Throwable {
	        
	    }

}
